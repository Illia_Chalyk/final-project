-- TODO update this script
DROP SCHEMA IF EXISTS `testing` ;
CREATE SCHEMA IF NOT EXISTS `testing` DEFAULT CHARACTER SET utf8 ;
USE `testing` ;

DROP TABLE IF EXISTS `testing`.`test_difficulty` ;
CREATE TABLE IF NOT EXISTS `testing`.`test_difficulty` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`test_status` ;
CREATE TABLE IF NOT EXISTS `testing`.`test_status` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `testing`.`test` ;
CREATE TABLE IF NOT EXISTS `testing`.`test` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64) NOT NULL,
    `subject` VARCHAR(64) NOT NULL,
    `difficulty` INT NOT NULL,
    `number_of_questions` INT NOT NULL DEFAULT 0,
    `time_limit_in_minutes` INT NOT NULL,
    `description` TEXT NOT NULL,
    `status` INT NOT NULL DEFAULT 1,
    PRIMARY KEY (`id`),
    INDEX `difficulty_idx` (`difficulty` ASC) VISIBLE,
    INDEX `test_status_idx` (`status` ASC) VISIBLE,
    UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
    CONSTRAINT `difficulty`
    FOREIGN KEY (`difficulty`)
    REFERENCES `testing`.`test_difficulty` (`id`)
    ON UPDATE RESTRICT,
    CONSTRAINT `test_status`
    FOREIGN KEY (`status`)
    REFERENCES `testing`.`test_status` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`question_type` ;
CREATE TABLE IF NOT EXISTS `testing`.`question_type` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;

DROP TABLE IF EXISTS `testing`.`question` ;
CREATE TABLE IF NOT EXISTS `testing`.`question` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `type` INT NOT NULL,
    `test_id` INT NOT NULL,
    `question_text` TEXT NOT NULL,
    `image_filename` TEXT NULL,
    PRIMARY KEY (`id`),
    INDEX `test_id` (`test_id` ASC) VISIBLE,
    INDEX `question_type_idx` (`type` ASC) VISIBLE,
    CONSTRAINT `test_id`
    FOREIGN KEY (`test_id`)
    REFERENCES `testing`.`test` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `question_type`
    FOREIGN KEY (`type`)
    REFERENCES `testing`.`question_type` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`answer` ;
CREATE TABLE IF NOT EXISTS `testing`.`answer` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `question_id` INT NOT NULL,
    `answer_text` TEXT NOT NULL,
    `is_correct` TINYINT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `question_id_idx` (`question_id` ASC) VISIBLE,
    CONSTRAINT `question_id`
    FOREIGN KEY (`question_id`)
    REFERENCES `testing`.`question` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`user_role` ;
CREATE TABLE IF NOT EXISTS `testing`.`user_role` (
    `id` TINYINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`user_status` ;
CREATE TABLE IF NOT EXISTS `testing`.`user_status` (
    `id` TINYINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`user` ;
CREATE TABLE IF NOT EXISTS `testing`.`user` (
     `id` INT NOT NULL AUTO_INCREMENT,
     `first_name` VARCHAR(32) NOT NULL,
    `last_name` VARCHAR(32) NOT NULL,
    `email` VARCHAR(320) NOT NULL,
    `password` VARCHAR(64) NOT NULL,
    `password_salt` VARCHAR(64) NOT NULL,
    `role` TINYINT NOT NULL DEFAULT '1',
    `status` TINYINT NOT NULL DEFAULT '1',
    `registration_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
    INDEX `status_idx` (`status` ASC) VISIBLE,
    INDEX `role_idx` (`role` ASC) VISIBLE,
    CONSTRAINT `role`
    FOREIGN KEY (`role`)
    REFERENCES `testing`.`user_role` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    CONSTRAINT `status`
    FOREIGN KEY (`status`)
    REFERENCES `testing`.`user_status` (`id`)
    ON UPDATE CASCADE)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`users_tests` ;
CREATE TABLE IF NOT EXISTS `testing`.`users_tests` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `test_id` INT NOT NULL,
    INDEX `user_id` (`user_id` ASC) VISIBLE,
    INDEX `test_id_idx` (`test_id` ASC) VISIBLE,
    PRIMARY KEY (`id`),
    CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `testing`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `ut_test_id`
    FOREIGN KEY (`test_id`)
    REFERENCES `testing`.`test` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `testing`.`attemp_statistic` ;
CREATE TABLE IF NOT EXISTS `testing`.`attemp_statistic` (
    `attempt_id` INT NOT NULL,
    `question_id` INT NOT NULL,
    `answer_id` INT NOT NULL,
    INDEX `attempt_id_idx` (`attempt_id` ASC) VISIBLE,
    INDEX `at_question_id_idx` (`question_id` ASC) VISIBLE,
    INDEX `at_answer_id_idx` (`answer_id` ASC) VISIBLE,
    CONSTRAINT `attempt_id`
    FOREIGN KEY (`attempt_id`)
    REFERENCES `testing`.`users_tests` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `at_question_id`
    FOREIGN KEY (`question_id`)
    REFERENCES `testing`.`question` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT `at_answer_id`
    FOREIGN KEY (`answer_id`)
    REFERENCES `testing`.`answer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
    ENGINE = InnoDB;

CREATE TRIGGER tr_question_insert
    AFTER INSERT ON question
    FOR EACH ROW
    UPDATE test SET number_of_questions = number_of_questions + 1
    WHERE test.id = NEW.test_id;

CREATE TRIGGER tr_question_detete
    AFTER DELETE ON question
    FOR EACH ROW
    UPDATE test SET number_of_questions = number_of_questions - 1
    WHERE test.id = OLD.test_id;

INSERT INTO user_status (name) VALUES ('active'), ('blocked');
INSERT INTO user_role (name) VALUES ('user'), ('admin');

INSERT INTO test_difficulty (name) VALUES ('easy'), ('medium'), ('hard');
INSERT INTO test_status (name) VALUES ('editing'), ('submitted'), ('archived');

INSERT INTO question_type (name) VALUES ('single'), ('multiple');
