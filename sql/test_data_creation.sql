INSERT INTO `answer`
VALUES (1, 1, 'Incorrect', 0),
       (2, 1, 'Incorrect', 0),
       (3, 1, 'Correct', 1),
       (4, 1, 'Incorrect', 0),
       (5, 2, 'Incorrect', 0),
       (6, 2, 'Incorrect', 0),
       (7, 2, 'Correct', 1),
       (8, 2, 'Incorrect', 0),
       (9, 3, 'Incorrect', 0),
       (10, 3, 'Incorrect', 0),
       (11, 3, 'Correct', 1),
       (12, 3, 'Incorrect', 0),
       (13, 4, 'Incorrect', 0),
       (14, 4, 'Incorrect', 0),
       (15, 4, 'Correct', 1),
       (16, 4, 'Incorrect', 0),
       (17, 5, 'Incorrect', 0),
       (18, 5, 'Incorrect', 0),
       (19, 5, 'Correct', 1),
       (20, 5, 'Incorrect', 0),
       (21, 6, 'Ok', 1),
       (22, 6, 'Not ok', 1),
       (23, 7, 's', 0),
       (24, 7, 'p', 1),
       (25, 7, 'first', 0);

INSERT INTO `question`
VALUES (1, 1, 1, 'Question 1', NULL),
       (2, 1, 1, 'Question 2', NULL),
       (3, 1, 1, 'Question 3', NULL),
       (4, 1, 1, 'Question 4', NULL),
       (5, 1, 1, 'Question 5', NULL),
       (6, 2, 2, 'How ate you?', NULL),
       (7, 1, 2, 'First letter in word \"Physics\" is', NULL);

INSERT INTO `test`
VALUES (1, 'Math test', 'Math', 2, 5, 10, 'This is not actual test', 2),
       (2, 'Physics test', 'Physics', 1, 2, 1, 'This is not a real test', 2),
       (3, 'Empty test 1', 'none', 1, 0, 1, 'Empty', 1),
       (4, 'Empty test 2', 'none', 1, 0, 1, 'Empty', 1),
       (5, 'Empty test 3', 'none', 1, 0, 1, 'Empty', 1),
       (6, 'Empty test 4', 'none', 1, 0, 1, 'Empty', 1),
       (7, 'Empty test 5', 'none', 1, 0, 1, 'Empty', 1),
       (8, 'Empty test 6', 'none', 1, 0, 1, 'Empty', 1),
       (9, 'Empty test 7', 'none', 1, 0, 1, 'Empty', 1),
       (10, 'Empty test 8', 'none', 1, 0, 1, 'Empty', 1),
       (11, 'Empty test 9', 'none', 1, 0, 1, 'Empty', 1),
       (12, 'Empty test 10', 'none', 1, 0, 1, 'Empty', 1),
       (13, 'Empty test 11', 'none', 1, 0, 1, 'Empty', 1),
       (14, 'Empty test 12', 'none', 1, 0, 1, 'Empty', 1),
       (15, 'Empty test 13', 'none', 1, 0, 1, 'Empty', 1),
       (16, 'Empty test 14', 'none', 1, 0, 1, 'Empty', 1),
       (17, 'Empty test 15', 'none', 1, 0, 1, 'Empty', 1),
       (18, 'Empty test 16', 'none', 1, 0, 1, 'Empty', 1),
       (19, 'Empty test 17', 'none', 1, 0, 1, 'Empty', 1),
       (20, 'Empty test 18', 'none', 1, 0, 1, 'Empty', 1),
       (21, 'Empty test 19', 'none', 1, 0, 1, 'Empty', 1),
       (22, 'Empty test 20', 'none', 1, 0, 1, 'Empty', 1),
       (23, 'Empty test 21', 'none', 1, 0, 1, 'Empty', 1),
       (24, 'Empty test 22', 'none', 1, 0, 1, 'Empty', 1),
       (25, 'Empty test 23', 'none', 1, 0, 1, 'Empty', 1),
       (26, 'Empty test 24', 'none', 1, 0, 1, 'Empty', 1);

INSERT INTO `user`
VALUES (1, 'Illia', 'Chalyk', 'root@root', 'root', 2, 1, '2021-02-10 15:10:58'),
       (2, 'John', 'Smith', 'john@gmail.com', '0000', 1, 1, '2021-02-10 15:13:21'),
       (3, 'Barak', 'Obama', 'obama@gmail.com', '0000', 1, 1, '2021-02-10 15:15:26'),
       (4, 'Lulu', 'Logam', 'lulu@gmail.com', '0000', 1, 1, '2021-02-10 15:17:14');

