package com.my.project.dao;

import com.my.project.domain.test.Answer;
import com.my.project.domain.test.Question;
import com.my.project.domain.test.Test;

import java.util.List;

public interface TestDAO {
    List<Test> getAllTests(int rows, int offset) throws DAOException;

    Test getTestById(int id) throws DAOException;

    void addQuestionToTest(Test test, Question question) throws DAOException;

    void addAnswerToQuestion(Question question, Answer answer) throws DAOException;

    void insertTest(Test test) throws DAOException;

    int getNumberOfTests() throws DAOException;

    void deleteTestDyId(int testId) throws DAOException;

    void updateTest(Test test) throws DAOException;

    //TODO extract those methods to QuestionDAO
    Question getQuestionById(int id) throws DAOException;

    void deleteAnswerById(int id) throws DAOException;

    void deleteQuestionById(int id) throws DAOException;

    void updateQuestion(Question question) throws DAOException;

    Answer getAnswerById(int id) throws DAOException;

    void updateAnswer(Answer answer) throws DAOException;
}
