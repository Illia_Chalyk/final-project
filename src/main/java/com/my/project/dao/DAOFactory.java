package com.my.project.dao;


public interface DAOFactory {
    UserDAO getUserDAO();

    TestDAO getTestDAO();

    TestResultDAO getTestResultDAO();
}
