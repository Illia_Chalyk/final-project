package com.my.project.dao;

import com.my.project.domain.user.User;

public interface UserDAO {
    void insertUser(User user) throws DAOException;

    User getUserByEmail(String email) throws DAOException;

    boolean isUserExist(User user) throws DAOException;
}
