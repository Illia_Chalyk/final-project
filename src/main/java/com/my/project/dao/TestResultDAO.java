package com.my.project.dao;

import com.my.project.domain.test.TestResult;
import com.my.project.domain.test.QuestionAnswerPair;
import com.my.project.domain.user.User;

import java.util.List;
import java.util.Set;

//TODO rename
public interface TestResultDAO {
    List<TestResult> getUserTestResults(User user) throws DAOException;

    void insertTestResult(TestResult testResult) throws DAOException;

    void insertUserAnswers(int attemptId, Set<QuestionAnswerPair> pairSet) throws DAOException;
}
