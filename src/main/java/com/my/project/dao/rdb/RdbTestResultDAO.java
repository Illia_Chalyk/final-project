package com.my.project.dao.rdb;

import com.my.project.dao.DAOException;
import com.my.project.dao.TestResultDAO;
import com.my.project.domain.test.TestResult;
import com.my.project.domain.test.QuestionAnswerPair;
import com.my.project.domain.user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

//TODO refactor this according to DB structure

public class RdbTestResultDAO implements TestResultDAO {
    @Override
    public List<TestResult> getUserTestResults(User user) throws DAOException {
        //TODO pagination
        List<TestResult> testResults = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT * FROM users_tests WHERE user_id = ?");
            statement.setInt(1, user.getId());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                TestResult testResult = mapTestResult(resultSet);
                //TODO scoring
                testResult.setScore(countScore(testResult.getId(), testResult.getTestId()));

                testResults.add(testResult);
            }

        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return testResults;
    }

    private TestResult mapTestResult(ResultSet resultSet) throws SQLException {
        TestResult testResult = new TestResult();
        testResult.setId(resultSet.getInt("id"));
        testResult.setUserId(resultSet.getInt("user_id"));
        testResult.setTestId(resultSet.getInt("test_id"));
        testResult.setAttemptDate(resultSet.getDate("attempt_date"));
        return testResult;
    }

    @Override
    public void insertTestResult(TestResult testResult) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("INSERT INTO users_tests (user_id, test_id) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, testResult.getUserId());
            statement.setInt(2, testResult.getTestId());
            if (statement.executeUpdate() > 0) {
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    testResult.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    //TODO refactor this
    private int countScore(int attemptId, int testId) {
        Connection connection = null;

        PreparedStatement statement = null;
        ResultSet resultSet = null;

        PreparedStatement statement2 = null;
        ResultSet resultSet2 = null;

        int numOfCorrectAnswers = 0;
        int userCorrectAnswers = 0;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();


            statement = connection.prepareStatement(
                    "SELECT count(0) AS correct_answers FROM answer JOIN question ON question.id = answer.question_id WHERE test_id = ? AND is_correct = true");
            statement.setInt(1, testId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                numOfCorrectAnswers = resultSet.getInt("correct_answers");
            }

            statement2 = connection.prepareStatement
                    ("SELECT count(0) AS score FROM attempt_statistic JOIN answer ON answer_id = answer.id AND is_correct = true WHERE attempt_id = ?");
            statement2.setInt(1, attemptId);
            resultSet2 = statement2.executeQuery();
            if (resultSet2.next()) {
                userCorrectAnswers = resultSet2.getInt("score");
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement2);
            RdbDAOFactory.close(resultSet2);
            RdbDAOFactory.close(connection);
        }
        int res = 0;
        if (userCorrectAnswers != 0 && numOfCorrectAnswers != 0) {
            res = Math.round(((float) userCorrectAnswers / numOfCorrectAnswers) * 100);
        }
        return res;
    }

    @Override
    public void insertUserAnswers(int attemptId, Set<QuestionAnswerPair> pairSet) throws DAOException {
        //TODO scoring
        //TODO single addition method as well (if needed)
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("INSERT INTO attempt_statistic VALUES (?, ?, ?)");
            statement.setInt(1, attemptId);
            for (QuestionAnswerPair pair : pairSet) {
                statement.setInt(2, pair.getQuestionId());
                statement.setInt(3, pair.getAnswerId());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            //TODO exception handling
            try {
                connection.rollback();
            } catch (SQLException e2) {
                //TODO exception handling
                e2.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }
}
