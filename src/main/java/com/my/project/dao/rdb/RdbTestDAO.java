package com.my.project.dao.rdb;

import com.my.project.dao.DAOException;
import com.my.project.dao.TestDAO;
import com.my.project.domain.test.*;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RdbTestDAO implements TestDAO {
    /**
     * Used for pagination.
     *
     * @return number of tests in the Database
     * @throws DAOException
     */
    @Override
    public int getNumberOfTests() throws DAOException {
        int num = 0;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.createStatement();
            statement.executeQuery("SELECT COUNT(id) AS count FROM test");
            resultSet = statement.getResultSet();
            if (resultSet.next()) {
                num = resultSet.getInt("count");
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return num;
    }

    @Override
    public List<Test> getAllTests(int rows, int offset) throws DAOException {
        //TODO add param fot pagination
        //  1) num of rows (limit)
        //  2) offset
        //  3) sorting column
        String rowToSortBy = "id"; //TODO add this to method params
        List<Test> tests = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            //TODO add sql constants
            statement = connection.prepareStatement("SELECT * FROM test ORDER BY ? LIMIT ? OFFSET ?");
            statement.setString(1, rowToSortBy);
            statement.setInt(2, rows);
            statement.setInt(3, offset);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Test test = mapTest(resultSet);
                test.setQuestions(getTestQuestions(test, connection));
                tests.add(test);
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return tests;
    }

    private List<Question> getTestQuestions(Test test, Connection connection) throws SQLException {
        List<Question> questions = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //TODO sql constants
            statement = connection.prepareStatement("SELECT * FROM question WHERE test_id = ? ORDER BY RAND()");
            statement.setInt(1, test.getId());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Question question = mapQuestion(resultSet);
                question.setAnswers(getQuestionAnswers(question, connection));
                questions.add(question);
            }
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
        }
        return questions;
    }

    private List<Answer> getQuestionAnswers(Question question, Connection connection) throws SQLException {
        List<Answer> answers = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //TODO sql constants
            statement = connection.prepareStatement("SELECT * FROM answer WHERE question_id = ? ORDER BY RAND()");
            statement.setInt(1, question.getId());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Answer answer = mapAnswer(resultSet);
                answers.add(answer);
            }
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
        }
        return answers;
    }


    private Answer mapAnswer(ResultSet resultSet) throws SQLException {
        Answer answer = new Answer();
        answer.setId(resultSet.getInt("id"));
        answer.setAnswerText(resultSet.getString("answer_text"));
        answer.setCorrect(resultSet.getBoolean("is_correct"));
        return answer;
    }

    private Question mapQuestion(ResultSet resultSet) throws SQLException {
        Question question = new Question();
        question.setId(resultSet.getInt("id"));
        question.setQuestionText(resultSet.getString("question_text"));
        String imgFilename = resultSet.getString("image_filename");
        if (imgFilename != null) {
            File file = new File(imgFilename);
            question.setImgFile(file);
        }
        return question;
    }

    private Test mapTest(ResultSet resultSet) throws SQLException {
        Test test = new Test();
        test.setId(resultSet.getInt("id"));
        test.setName(resultSet.getString("name"));
        test.setSubject(resultSet.getString("subject"));
        int difficultyId = resultSet.getInt("difficulty") - 1;
        test.setDifficulty(TestDifficulty.values()[difficultyId]);
        test.setNumberOfQuestions(resultSet.getInt("number_of_questions"));
        test.setTimeLimitInMinutes(resultSet.getInt("time_limit_in_minutes"));
        test.setDescription(resultSet.getString("description"));
        int statusId = resultSet.getInt("status") - 1;
        test.setStatus(TestStatus.values()[statusId]);
        return test;
    }

    @Override
    public Test getTestById(int id) throws DAOException {
//        throw new UnsupportedOperationException("Method not yet implemented");
        Test test = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            //TODO  1) refactor this verbose sql query
            //      2) add sql constants
            statement = connection.prepareStatement("SELECT * FROM test WHERE id = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                test = mapTest(resultSet);
                test.setQuestions(getTestQuestions(test, connection));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO exception handling
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return test;
    }

    @Override
    public void insertTest(Test test) throws DAOException {
        //TODO add test fields validation before inserts
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement(
                    "INSERT INTO test (name, subject, difficulty, time_limit_in_minutes, description) " +
                            "VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, test.getName());
            statement.setString(2, test.getSubject());
            statement.setInt(3, test.getDifficulty().ordinal() + 1);
            statement.setInt(4, test.getTimeLimitInMinutes());
            statement.setString(5, test.getDescription());
            if (statement.executeUpdate() > 0) {
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    test.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public void addQuestionToTest(Test test, Question question) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        //TODO logic...
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            //TODO consider question type
            statement = connection.prepareStatement(
                    "INSERT INTO question (test_id, question_text) VALUES (?, ?)");
            statement.setInt(1, test.getId());
            statement.setString(2, question.getQuestionText());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public void addAnswerToQuestion(Question question, Answer answer) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement(
                    "INSERT INTO answer (question_id, answer_text, is_correct) VALUES (?, ?, ?)");
            statement.setInt(1, question.getId());
            statement.setString(2, answer.getAnswerText());
            statement.setBoolean(3, answer.getCorrect());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public void deleteTestDyId(int testId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM test WHERE id = ?");
            statement.setInt(1, testId);
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public void updateTest(Test test) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement(
                    "UPDATE test " +
                            "SET name = ?, subject = ?, difficulty = ?, time_limit_in_minutes = ?, description = ?, status = ? " +
                            "WHERE id = ?");
            statement.setString(1, test.getName());
            statement.setString(2, test.getSubject());
            statement.setInt(3, test.getDifficulty().ordinal() + 1);
            statement.setInt(4, test.getTimeLimitInMinutes());
            statement.setString(5, test.getDescription());
            statement.setInt(6, test.getStatus().ordinal() + 1);
            statement.setInt(7, test.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    //TODO extract this method to QuestionDAO
    @Override
    public Question getQuestionById(int id) throws DAOException {
        Question question = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT * FROM question WHERE id = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                question = mapQuestion(resultSet);
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return question;
    }

    @Override
    public void deleteAnswerById(int id) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM answer WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public void deleteQuestionById(int id) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("DELETE FROM question WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public void updateQuestion(Question question) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement(
                    "UPDATE question SET question_text = ? WHERE id = ?");
            statement.setString(1, question.getQuestionText());
            statement.setInt(2, question.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public Answer getAnswerById(int id) throws DAOException {
        Answer answer = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT * FROM answer WHERE id = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                answer = mapAnswer(resultSet);
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return answer;
    }

    @Override
    public void updateAnswer(Answer answer) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            //TODO add more parameters to update
            statement = connection.prepareStatement(
                    "UPDATE answer SET answer_text = ?, is_correct = ? WHERE id = ?");
            statement.setString(1, answer.getAnswerText());
            statement.setBoolean(2, answer.getCorrect());
            statement.setInt(3, answer.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }
}
