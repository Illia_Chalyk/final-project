package com.my.project.ui.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Authenticator extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String loginURL = req.getContextPath() + "/login";
        String registrationURL = req.getContextPath() + "/registration";
        String reqURL = req.getRequestURI();

        if (reqURL.equals(loginURL) || reqURL.equals(registrationURL)) {
            chain.doFilter(req, res);
            return;
        }

        HttpSession session = req.getSession();
        Boolean isLogged = (Boolean) session.getAttribute("isLogged");

        if (isLogged == null || !isLogged) {
            res.sendRedirect("login");
            return;
        }
        chain.doFilter(req, res);
    }
}
