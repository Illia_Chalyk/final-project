package com.my.project.ui.user;

import com.my.project.dao.DAOException;
import com.my.project.dao.TestResultDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.TestResult;
import com.my.project.domain.user.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RdbDAOFactory factory = RdbDAOFactory.getInstance();
        TestResultDAO testResultDAO = factory.getTestResultDAO();
        User user = (User) req.getSession().getAttribute("user");
        try {
            //TODO add pagination
            List<TestResult> results = testResultDAO.getUserTestResults(user);
            req.setAttribute("results", results);
        } catch (DAOException e) {
            //TODO exception handling;
            e.printStackTrace();
        }
        req.getRequestDispatcher("/profile.jsp").forward(req, res);
    }
}
