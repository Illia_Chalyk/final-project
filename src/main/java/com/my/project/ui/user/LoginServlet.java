package com.my.project.ui.user;

import com.my.project.dao.DAOException;
import com.my.project.dao.UserDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.user.User;
import com.my.project.servises.PasswordProcessor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") == null) {
            req.getRequestDispatcher("/login.jsp").forward(req, res);
        } else {
            res.sendRedirect("homepage");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //TODO request parameters validation
        UserDAO userDAO = RdbDAOFactory.getInstance().getUserDAO();
        User user = null;
        PasswordProcessor passwordProcessor = new PasswordProcessor();
        try {
            user = userDAO.getUserByEmail(req.getParameter("email"));
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
            res.sendRedirect("login");
            return;
        }
        //TODO refactor this
        if (!passwordProcessor.comparePasswords(req.getParameter("password"), user.getPassword(), user.getPasswordSalt())) {
            res.sendRedirect("login");
            return;
        }
        //TODO Duplicating code (#001)
        HttpSession session = req.getSession();
        //TODO remove isLogged attribute
        session.setAttribute("isLogged", true);
        session.setAttribute("user", user);
        session.setAttribute("locale", req.getLocale());
        res.sendRedirect("homepage");
    }
}
