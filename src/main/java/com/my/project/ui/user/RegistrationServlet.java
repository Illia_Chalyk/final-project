package com.my.project.ui.user;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.UserDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.user.User;
import com.my.project.servises.PasswordProcessor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") == null) {
            req.getRequestDispatcher("/registration.jsp").forward(req, res);
        } else {
            res.sendRedirect("homepage");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //TODO request parameters validation
        DAOFactory factory = RdbDAOFactory.getInstance();
        UserDAO userDAO = factory.getUserDAO();

        //TODO constants for request parameters
        User user = new User();
        user.setFirstName(req.getParameter("firstName"));
        user.setLastName(req.getParameter("lastName"));
        user.setEmail(req.getParameter("email"));

        PasswordProcessor passwordProcessor = new PasswordProcessor();
        String[] passAndSalt = passwordProcessor.hashPassword(req.getParameter("password"));
        String hashedPassword = passAndSalt[0];
        String passwordSalt = passAndSalt[1];
        user.setPassword(hashedPassword);
        user.setPasswordSalt(passwordSalt);
        try {
            userDAO.isUserExist(user);
            userDAO.insertUser(user);
        } catch (DAOException e) {
            //TODO  1) exception handling
            //      2) show a user an error message
            e.printStackTrace();
            res.sendRedirect("registration");
        }
        req.getRequestDispatcher("login").forward(req, res);
    }
}
