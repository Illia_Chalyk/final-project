package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TestInfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int testId = Integer.parseInt(req.getParameter("testId"));
        TestDAO testDAO = RdbDAOFactory.getInstance().getTestDAO();
        Test test = null;
        try {
            test = testDAO.getTestById(testId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        req.setAttribute("test", test);
        req.getRequestDispatcher("testinfo.jsp").forward(req, res);
    }
}
