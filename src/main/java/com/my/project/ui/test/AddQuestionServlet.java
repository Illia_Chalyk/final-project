package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Question;
import com.my.project.domain.test.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddQuestionServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Question question = new Question();
        question.setQuestionText(req.getParameter("questionText"));
        int testId = Integer.parseInt(req.getParameter("testId"));

        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        //TODO refactor this
        Test test = new Test();
        test.setId(testId);
        try {
            testDAO.addQuestionToTest(test, question);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            res.sendRedirect("testedit?testId=" + test.getId());
        }
    }
}
