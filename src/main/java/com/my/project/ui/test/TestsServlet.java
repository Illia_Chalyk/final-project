package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TestsServlet extends HttpServlet {
    //TODO choose doGet or doPost
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //TODO refactor this method
        List<Test> tests = null;
        int recordsPerPage = 10;
        String currentPageAtr = req.getParameter("currentPage");
        int currentPage = 0;
        if (currentPageAtr == null) {
            currentPage = 1;
        } else {
            currentPage = Integer.parseInt(req.getParameter("currentPage"));
        }
        int numOfPages = 0;
        try {
            RdbDAOFactory factory = RdbDAOFactory.getInstance();
            TestDAO testDAO = factory.getTestDAO();
            tests = testDAO.getAllTests(recordsPerPage, currentPage * recordsPerPage - recordsPerPage);
            int numOfTests = testDAO.getNumberOfTests();
            numOfPages = (int) Math.ceil((double) numOfTests / recordsPerPage);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        //TODO  2) add sorting method
        req.setAttribute("tests", tests);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("recordsPerPage", recordsPerPage);
        req.setAttribute("numOfPages", numOfPages);
        req.getRequestDispatcher("tests.jsp").forward(req, res);
    }
}
