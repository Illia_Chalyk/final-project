package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Test;
import com.my.project.domain.test.TestDifficulty;
import com.my.project.domain.test.TestStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TestEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //TODO refactor duplicate block of code
        int testId = Integer.parseInt(req.getParameter("testId"));
        TestDAO testDAO = RdbDAOFactory.getInstance().getTestDAO();
        Test test = null;
        try {
            test = testDAO.getTestById(testId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        req.setAttribute("test", test);
        req.getRequestDispatcher("/testedit.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //TODO add validation
        int testId = Integer.parseInt(req.getParameter("testId"));
        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        Test test = null;
        try {
            test = testDAO.getTestById(testId);
            String submit = req.getParameter("submit");
            if (submit != null) {
                test.setStatus(TestStatus.SUBMITTED);
            } else {
                test.setName(req.getParameter("testName"));
                test.setSubject(req.getParameter("subject"));
                test.setDifficulty(TestDifficulty.valueOf(req.getParameter("difficulty")));
                test.setTimeLimitInMinutes(Integer.parseInt(req.getParameter("timeLimitInMinutes")));
                test.setDescription(req.getParameter("description"));
            }
            testDAO.updateTest(test);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        res.sendRedirect("testedit?testId=" + testId);
    }
}
