package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Answer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditAnswerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Answer answer = null;
        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        //TODO refactor this logic
        int answerId = Integer.parseInt(req.getParameter("answerId"));
        String correctAtr = req.getParameter("correct");
        boolean correct = false;
        if(correctAtr != null){
            correct = true;
        }
        try {
            answer = testDAO.getAnswerById(answerId);
            answer.setAnswerText(req.getParameter("answerText"));
            answer.setCorrect(correct);
            testDAO.updateAnswer(answer);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        res.sendRedirect("testedit?testId=" + req.getParameter("testId"));
    }
}
