package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Test;
import com.my.project.domain.test.TestDifficulty;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddTestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("/addtest.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Test test = new Test();
        test.setName(req.getParameter("testName"));
        test.setSubject(req.getParameter("subject"));
        test.setDifficulty(TestDifficulty.valueOf(req.getParameter("difficulty").toUpperCase()));
        test.setTimeLimitInMinutes(Integer.parseInt(req.getParameter("timeLimitInMinutes")));
        test.setDescription(req.getParameter("description"));

        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        try {
            testDAO.insertTest(test);
            res.sendRedirect("testedit?testId=" + test.getId());
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }
}
