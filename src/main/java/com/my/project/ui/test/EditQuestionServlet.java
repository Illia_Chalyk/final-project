package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Question;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditQuestionServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Question question = null;
        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        int questionId = Integer.parseInt(req.getParameter("questionId"));
        try {
            question = testDAO.getQuestionById(questionId);
            question.setQuestionText(req.getParameter("questionText"));
            testDAO.updateQuestion(question);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        res.sendRedirect("testedit?testId=" + req.getParameter("testId"));
    }
}
