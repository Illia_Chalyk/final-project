package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.DAOFactory;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteAnswerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        int answerId = Integer.parseInt(req.getParameter("answerId"));
        try {
            testDAO.deleteAnswerById(answerId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        res.sendRedirect("testedit?testId=" + req.getParameter("testId"));
    }
}
