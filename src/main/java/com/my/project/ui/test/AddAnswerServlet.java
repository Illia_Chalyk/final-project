package com.my.project.ui.test;

import com.my.project.dao.DAOException;
import com.my.project.dao.TestDAO;
import com.my.project.dao.rdb.RdbDAOFactory;
import com.my.project.domain.test.Answer;
import com.my.project.domain.test.Question;
import com.my.project.domain.test.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddAnswerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //TODO add validation of parameters here
        // (and to other similar methods)
        Answer answer = new Answer();
        answer.setAnswerText(req.getParameter("answerText"));
        answer.setCorrect(Boolean.parseBoolean(req.getParameter("correct")));

        int questionId = Integer.parseInt(req.getParameter("questionId"));
        TestDAO testDAO = RdbDAOFactory.getInstance().getTestDAO();
        Question question = null;
        try {
            question = testDAO.getQuestionById(questionId);
            testDAO.addAnswerToQuestion(question, answer);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        res.sendRedirect("testedit?testId=" + req.getParameter("testId"));
    }
}
