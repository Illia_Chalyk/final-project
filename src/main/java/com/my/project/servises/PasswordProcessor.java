package com.my.project.servises;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordProcessor {
    //WARNING!!! DO NOT CHANGE THOSE VALUES!!!
    private static final byte[] PEPPER = {
            -1, -10, 64, 112, 85, -62, -37, 41, 6, -38, -121,
            113, -40, 39, -20, 120, 65, 72, -108, 21, 116, 25,
            -83, -84, 66, -85, -98, -112, 23, -99, 24, -126,
            -3, 95, -65, 60, -121, -18, 36, -110, 28, -51,
            116, -49, -18, -40, -45, 69, -10, 72, -11, 36, -52,
            75, 98, 48, 77, 80, -115, -90, -120, 31, -58, -46
    };

    /**
     * Hashes a password with salt and pepper.
     *
     * @param passwordToHash - plain text password.
     * @return String[] with two values [0] index is hashed password [1] index is salt.
     */
    public String[] hashPassword(String passwordToHash) {
        final String salt = String.valueOf(Math.random());
        String hashedPassword = hashPassword0(passwordToHash, salt);
        return new String[]{hashedPassword, salt};
    }

    private String hashPassword0(String passwordToHash, String salt) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("sha-512");
        } catch (NoSuchAlgorithmException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        //DO NOT TOUCH THIS THREE md.update()!!!
        md.update(passwordToHash.getBytes(StandardCharsets.UTF_8));
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        md.update(PEPPER);
        String hashedPassword = new String(md.digest());
        return hashedPassword;
    }

    /**
     * @param password       - plain text password to check.
     * @param hashedPassword - hashed password.
     * @param salt           - salt that has been used for hashed password.
     * @return true if the hashes match.
     */
    public boolean comparePasswords(final String password, final String hashedPassword, final String salt) {
        String hashed = hashPassword0(password, salt);
        return hashed.equals(hashedPassword);
    }
}
