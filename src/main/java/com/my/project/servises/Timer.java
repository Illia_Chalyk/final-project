package com.my.project.servises;

/**
 * Timer used to limit the time for passing a test
 */
public class Timer {
    private final long expiredTime;

    public Timer(int timeLimitInMinutes) {
        long startTime = System.currentTimeMillis();
        long timeLimitInMillis = (long) timeLimitInMinutes * 60 * 1000;
        this.expiredTime = startTime + timeLimitInMillis;
    }

    /**
     * @return true if is still time left
     */
    public boolean isActive() {
        long currentTime = System.currentTimeMillis();
        return currentTime < expiredTime;
    }
}
