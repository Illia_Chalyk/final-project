package com.my.project.domain.test;

public enum TestDifficulty {
    EASY,
    MEDIUM,
    HARD
}
