package com.my.project.domain.test;

public enum TestStatus {
    EDITING,
    SUBMITTED,
    ARCHIVED
}
