package com.my.project.domain.test;

import java.sql.Date;

public class TestResult {
    private int id;
    private int userId;
    private int testId;
    private int score;
    private Date attemptDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getAttemptDate() {
        return attemptDate;
    }

    public void setAttemptDate(Date attemptDate) {
        this.attemptDate = attemptDate;
    }

    @Override
    public String toString() {
        return "TestResult{" +
                "id=" + id +
                ", userId=" + userId +
                ", testId=" + testId +
                ", score=" + score +
                ", attemptDate=" + attemptDate +
                '}';
    }
}
