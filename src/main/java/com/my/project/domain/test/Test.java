package com.my.project.domain.test;

import java.util.ArrayList;
import java.util.List;

public class Test {
    private int id;
    private String name;
    private String subject;
    private TestDifficulty difficulty;
    private int numberOfQuestions;
    private int timeLimitInMinutes;
    private String description;
    private TestStatus status;
    private List<Question> questions;

    public Test() {
        questions = new ArrayList<>();
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public TestDifficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(TestDifficulty difficulty) {
        this.difficulty = difficulty;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public int getTimeLimitInMinutes() {
        return timeLimitInMinutes;
    }

    public void setTimeLimitInMinutes(int timeLimitInMinutes) {
        this.timeLimitInMinutes = timeLimitInMinutes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TestStatus getStatus() {
        return status;
    }

    public void setStatus(TestStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", subject='" + subject + '\'' +
                ", difficulty=" + difficulty +
                ", numberOfQuestions=" + numberOfQuestions +
                ", timeLimitInMinuets=" + timeLimitInMinutes +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", questions=" + questions +
                '}';
    }
}

