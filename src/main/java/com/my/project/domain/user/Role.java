package com.my.project.domain.user;

public enum Role {
    USER,
    ADMIN;
}
