package com.my.project.domain.user;

public enum Status {
    ACTIVE,
    BLOCKED;
}
