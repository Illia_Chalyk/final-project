<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Tests passing" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%-- TODO nav through test --%>
<%--<table>--%>
<%--    <c:forEach var="i" begin="0" end="${test.questions.size() - 1}" step="1">--%>
<%--        <a href="passing?">${i+1} </a>--%>
<%--    </c:forEach>--%>
<%--</table>--%>
<br>
<br>
<br>
${test.questions.get(currentQuestion).questionText}
<br>
<br>
<form action="passing" method="post">
    <input hidden name="testId" value="${test.id}">
    <input hidden name="questionId" value="${test.questions.get(currentQuestion).id}">
    <c:forEach var="answer" items="${test.questions.get(currentQuestion).answers}">
        <%-- TODO show to user his answers while skimming through test --%>
        <input type="checkbox" name="ans" value="${answer.id}">${answer.answerText}<br>
    </c:forEach>
    <input type="submit" value="Save answer">
</form>
<br>
<br>
<form action="passing" method="get">
    <c:if test="${currentQuestion > 0}">
        <input type="submit" name="previous" value="Previous question">
    </c:if>
    <c:if test="${currentQuestion < test.questions.size() -1}">
        <input type="submit" name="next" value="Next question">
    </c:if>
</form>
<br>
<br>
<form action="passing" method="post">
    <input type="submit" name="submitTest" value="Submit the test">
</form>

</body>
</html>
