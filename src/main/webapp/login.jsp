<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Homepage" scope="page"/>
<%@include file="head.jspf" %>
<body>
<div>
    <div style="max-width: 400px;margin: auto;" class="text-center mt-5">
        <h1>Testing platform</h1>
    </div>
    <div style="max-width: 400px;height: 400px ;margin: auto;" class="text-center mt-5 bg-light">
        <form style="max-width: 360px;margin: auto;" action="login" method="post">
            <h1 class="display-6 pt-3 mb-3"><fmt:message key="login_jsp.login_form.label"/></h1>
            <div class="form-floating mb-3">
                <input class="form-control" type="email" name="email" id="floatingInput" placeholder="Email address"
                       required autofocus>
                <label for="floatingInput"><fmt:message key="login_jsp.login_form.placeholder.email"/></label>
            </div>
            <div class="form-floating mb-4">
                <input class="form-control" type="password" name="password" id="floatingPassword" placeholder="Password"
                       required="">
                <label for="floatingPassword"><fmt:message key="login_jsp.login_form.placeholder.password"/></label>
            </div>
            <div class="d-grid">
                <input class="btn btn-lg btn-dark btn-block" type="submit"
                       value="<fmt:message key="login_jsp.login_form.button"/>">
            </div>
        </form>
        <div class="mt-5">
            <!-- TODO add hrefs -->
            <a href="#"><fmt:message key="login_jsp.login_form.forgot_password"/></a>
            <span>&#9679;</span>
            <a href="registration"><fmt:message key="login_jsp.login_form.to_registration"/></a>
        </div>
    </div>
</div>
</body>
</html>
