<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Tests info" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%@include file="header.jspf" %>
<div align="center">

    <form action="deletetest?testId=${test.id}" method="post">
        <input type="submit" value="Delete test">
    </form>

    <br>
    <br>
    <form action="testedit?testId=${test.id}" method="post">
        Name: <input type="text" name="testName" value="${test.name}" required><br>
        Subject: <input type="text" name="subject" value="${test.subject}" required><br>
        Test difficulty: <select name="difficulty" required><br>
        <%-- TODO refactor this --%>
        <c:choose>
            <c:when test="${test.difficulty.toString().equals('EASY')}">
                <option selected>EASY</option>
                <option>MEDIUM</option>
                <option>HARD</option>
            </c:when>
            <c:when test="${test.difficulty.toString().equals('MEDIUM')}">
                <option>EASY</option>
                <option selected>MEDIUM</option>
                <option>HARD</option>
            </c:when>
            <c:when test="${test.difficulty.toString().equals('HARD')}">
                <option selected>EASY</option>
                <option>MEDIUM</option>
                <option selected>HARD</option>
            </c:when>
        </c:choose>
    </select><br>
        Number of questions: <input type="text" name="numberOfQuestions" value="${test.numberOfQuestions}" disabled><br>
        Time limit in minutes: <input type="number" min="1" max="300" name="timeLimitInMinutes"
                                      value="${test.timeLimitInMinuets}" required><br>
        Description: <textarea type="text" name="description" required>${test.description}</textarea><br>
        <input type="submit" value="Save changes"><br>
    </form>
    <br>
    <br>
    <br>
    <form action="testedit?testId=${test.id}" method="post">
        <input type="hidden" name="submit" value="">
        <input type="submit" value="Submit test">
    </form>

    <br>
    <br>
    <br>
    <form action="addquestion?testId=${test.id}" method="post">
        <input type="text" name="questionText" placeholder="Question text" required>
        <input type="submit" value="Add question">
    </form>
    <br>
    <br>
    <br>

    <c:forEach var="question" items="${test.questions}">
        <table>
            <form action="editquestion?testId=${test.id}&questionId=${question.id}" method="post">
                <input type="text" name="questionText" value="${question.questionText}" required>
                <input type="submit" value="Edit question">
            </form>
            <form action="deletequestion?testId=${test.id}&questionId=${question.id}" method="post">
                <input type="submit" value="Delete question">
            </form>
        </table>
        <br>
        <c:forEach var="answer" items="${question.answers}">
            <table>
                <form action="editanswer?testId=${test.id}&answerId=${answer.id}" method="post">
                    <input type="text" name="answerText" value="${answer.answerText}" required>
                    <c:choose>
                        <c:when test="${answer.correct}">
                            <input type="checkbox" name="correct" checked="checked" value="${answer.correct}">Is correct
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" name="correct" value="${answer.correct}">Is correct
                        </c:otherwise>
                    </c:choose>
                    <input type="submit" value="Edit answer">
                </form>
                <form action="deleteanswer?testId=${test.id}&answerId=${answer.id}" method="post">
                    <input type="submit" value="Delete answer">
                </form>
            </table>
            <br>
        </c:forEach>
        <form action="addanswer?testId=${test.id}&questionId=${question.id}" method="post">
            <input type="text" name="answerText" placeholder="Answer text" required>
            <input type="checkbox" name="correct" value="true">Is correct
            <input type="submit" value="Add answer">
        </form>
        <br>
        <br>
        <br>
    </c:forEach>
</div>
</body>
</html>