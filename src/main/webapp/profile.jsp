<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Profile" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%@include file="header.jspf" %>
<div align="center">
    <c:out value="First name: ${user.firstName}"/><br>
    <c:out value="Last name: ${user.lastName}"/><br>
    <c:out value="Email: ${user.email}"/><br>
    <c:out value="Registration date: ${user.registrationDate}"/><br>
    <c:out value="Role: ${user.role}"/><br>
</div>
<br>
<div align="center">
    <c:forEach var="result" items="${results}">
        <p>Test id: ${result.testId}</p>
        <p>Score: ${result.score}</p>
        <br>
    </c:forEach>
</div>
</body>
</html>
